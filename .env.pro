# 线上环境
NODE_ENV = 'production'

# 线上环境接口地址
#VITE_BASE_API = 'https://ai-server.beifuting.com'
VITE_BASE_API = 'https://ai-server.skin360.com.cn'

VITE_BASE_HTTP = 'https://ai-server-web.skin360.com.cn'
VITE_BASE_PY_API = 'https://ai-workflow.beifuting.com'
VITE_BASE_PY_SOCKIT = 'wss://ai-workflow.beifuting.com'
VITE_BASE_PY_WEBUPLOAD = 'https://ai-workflow-tool.beifuting.com'

