import { createApp } from 'vue'
import { createPinia } from 'pinia'
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'
import App from './App.vue'
import router from './router'
import 'element-plus/dist/index.css'
import '@/assets/css/index.scss'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import { errorHandler, detectOS, digits, getBrowserInfo, format } from '@/assets/scripts/errorPlugin' // 收集错误信息

const app = createApp(App)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}
const pinia = createPinia()
pinia.use(piniaPluginPersistedstate)
app.use(pinia)
app.use(router)
// 1. 用于组件生命周期中的错误、自定义事件处理函数内部错误、v-on DOM 监听器内部抛出的错误、处理返回 Promise 链的错误
app.config.errorHandler = errorHandler
// 2. 处理 JS 的额外错误
// eslint-disable-next-line max-params
window.onerror = function (message, source, line, column, error) {
    // 了解文档: https://juejin.cn/post/6844904093979246606
    let errMsg = null
    if (message == 'Script error.') {
        // 跨域
        errMsg = `
        --infoType: JS 无法访问, 请在控制台查看具体错误
        --apName : 用户端-${process.env.NODE_ENV === 'development' ? '测试环境' : '生产环境'}
        --url: ${window.location.href}
        --browser:${detectOS()}-${digits()} ${getBrowserInfo()}
        --time: ${format('yyyy-MM-dd hh:mm:ss')}
        --userInfo: ${sessionStorage.getItem('AI_INFO')}
        --info: 浏览器跨域请求一个脚本执行出错
        `
        return false
    }
    errMsg = `
        --infoType: JS 错误
        --apName : 用户端-${process.env.NODE_ENV === 'development' ? '测试环境' : '生产环境'}
        --url: ${window.location.href}
        --browser:${detectOS()}-${digits()} ${getBrowserInfo()}
        --time: ${format('yyyy-MM-dd hh:mm:ss')}
        --userInfo: ${sessionStorage.getItem('AI_INFO')}
        --info: ${message}-${source}-${JSON.stringify(error)}
        `
    errorHandler(errMsg, null, 'JS错误')
}
// 3. 处理 Promise 错误
window.addEventListener('unhandledrejection', (event) => {
    console.log('event', event.reason)
    // 全局存在的未处理的 Promise 异常，比如: Promise.reject()
    // 场景: 接口异常
    let errMsg = `
    --infoType: 捕获Promise异常
    --apName : 用户端-${process.env.NODE_ENV === 'development' ? '测试环境' : '生产环境'}
    --url: ${window.location.href}
    --browser:${detectOS()}-${digits()} ${getBrowserInfo()}
    --time: ${format('yyyy-MM-dd hh:mm:ss')}
    --userInfo: ${sessionStorage.getItem('AI_INFO')}
    --errorInfo: ${JSON.stringify(event.reason)}
    `

    errorHandler(errMsg, null, 'Promise错误')
})
app.mount('#app')
