import { defineStore } from 'pinia'

export const useContentStore = defineStore('content', {
    state: () => {
        return {
            editAssetObject: null
        }
    },
    actions: {}
})
