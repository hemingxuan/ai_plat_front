import { defineStore } from 'pinia'

export const usePersistStore = defineStore('persist', {
    state: () => {
        return {
            token: false,
            menusList: [],
            userInfo: {},
            taskObj: {} // 小工具中上传任务
        }
    },
    actions: {},
    persist: true
})
