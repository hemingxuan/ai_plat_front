import request from '../request'

// 登录
export function login(data) {
    return request({
        url: '/login',
        method: 'post',
        data
    })
}
// 登出
export function loginOut(data) {
    return request({
        url: '/loginOut',
        method: 'post',
        data
    })
}
