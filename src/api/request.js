import axios from 'axios'
import { ElMessage, ElLoading } from 'element-plus'
import router from '../router/index'

const service = axios.create({
    baseURL: import.meta.env.VITE_BASE_API,
    timeout: 50000
})

let loadingCount = 0
let loadingStatus = null

function showLoading() {
    if (loadingCount == 0) {
        loadingStatus = ElLoading.service({
            lock: true,
            text: 'Loading',
            background: 'rgba(0, 0, 0, 0.7)'
        })
    }
    loadingCount++
}

function hideLoading() {
    loadingCount--
    if (loadingStatus && loadingCount == 0) {
        loadingStatus.close()
        loadingStatus = null
    }
}

service.interceptors.request.use((config) => {
    if (config.isLoading) {
        showLoading()
    }
    let userInfo = sessionStorage.getItem('AI_INFO') || null
    config.headers.Authorization = sessionStorage.getItem('AI_TOKEN') || null
    if (userInfo) {
        config.headers.userId = JSON.parse(userInfo).id
    }
    return config
})
service.interceptors.response.use(
    (res) => {
        if (loadingStatus) {
            hideLoading()
        }
        if (res.data.success) return Promise.resolve(res.data.model)
        ElMessage.error({
            message: res.data.errorMessage || 'Error'
        })
        return Promise.reject(res.data)
    },
    (error) => {
        if (loadingStatus) {
            hideLoading()
        }
        if (error.response.data.status == 401) {
            sessionStorage.clear()
            localStorage.clear()
            router.replace('/login')
        } else {
            // 除 401外, 其他错误信息需提示
            ElMessage.error({
                message: error
            })
        }

        return Promise.reject(error)
    }
)

export default service
