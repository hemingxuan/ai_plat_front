import request from '../request'

//校验是否存在排队中或正在训练的风格资产
export function styleCheckTrainStatus() {
    return request({
        url: '/style/checkTrainStatus',
        method: 'get'
    })
}
// 开始训练
export function styleAddStyleTrain(data) {
    return request({
        url: '/style/addStyleTrain',
        method: 'post',
        data
    })
}

// 我训练的资产分页查询
export function findStyleTrainPageList(data) {
    return request({
        url: '/style/findStyleTrainPageList',
        method: 'post',
        data
    })
}
// 重新训练
export function retryTrain(data) {
    return request({
        url: '/style/retryTrain',
        method: 'post',
        data
    })
}
// 训练完成
export function trainComplete(data) {
    return request({
        url: '/style/trainComplete',
        method: 'post',
        data
    })
}

// 取消排队
export function cancelQueue(data) {
    return request({
        url: '/style/cancelQueue',
        method: 'post',
        data
    })
}
