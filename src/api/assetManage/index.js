import request from '../request'
import { params } from '@/assets/scripts/util.js'

// 上传地址
export function bigfileUpload(data) {
    return request({
        url: '/bigfile/upload',
        method: 'post',
        data
    })
}
// 检测文件是否存在url 或者 检测分片url
export function bigfileCheck(data) {
    return request({
        url: '/bigfile/check',
        method: 'post',
        data
    })
}
// 合并文件请求地址
export function bigfileMerge(data) {
    return request({
        url: '/bigfile/merge',
        method: 'post',
        data
    })
}

// 资产广场查询
export function findAssetPublicList(data) {
    return request({
        url: `/asset/findAssetPublicList`,
        method: 'post',
        data
    })
}

// 创建资产
export function addAsset(data) {
    return request({
        url: '/asset/addAsset',
        method: 'post',
        data
    })
}

// 根据code查询资产类型或标签
export function findAssetTypeOrTag(data) {
    return request({
        url: `/asset/findAssetTypeOrTag${params(data)}`,
        method: 'get',
        data
    })
}
// 收藏资产
export function collectAsset(data) {
    return request({
        url: `/asset/collectAsset`,
        method: 'post',
        data
    })
}
// 取消收藏资产
export function cancelCollect(data) {
    return request({
        url: `/asset/cancelCollect${params(data)}`,
        method: 'get'
    })
}
// 我上传的界面发布或取消发布
export function publishOrCancel(data) {
    return request({
        url: `/asset/publishOrCancel`,
        method: 'post',
        data
    })
}
// 我的资产–我收藏的或我上传的
export function findOwnOrCollect(data) {
    return request({
        url: `/asset/findOwnOrCollect`,
        method: 'post',
        data
    })
}
// 根据id查询资产详情
export function findAssetById(data) {
    return request({
        url: `/asset/findAssetById/${data}`,
        method: 'get'
    })
}
// 修改资产详情
export function updateAsset(data) {
    return request({
        url: `/asset/updateAsset`,
        method: 'post',
        data
    })
}
