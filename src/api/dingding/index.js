import request from '../request'

// 获取钉钉机器人报警完整url
export function getRobotUrl(data) {
    return request({
        url: '/ding/talk/getRobotUrl',
        method: 'post',
        data
    })
}
