import request from '../request'

//节点流列表
export function findPageList(params) {
    return request({
        url: '/node/flow/findPageList',
        method: 'get',
        params
    })
}

//根据id查询节点流详情
export function findNodeById(id) {
    return request({
        url: `/node/flow/findNodeById/${id}`,
        method: 'get'
    })
}

// 保存或packup节点流
export function saveNodeFlow(data) {
    return request({
        url: '/node/flow/saveNodeFlow',
        method: 'post',
        data,
        isLoading: true
    })
}

// 复制或另存为
export function copyOrSaveAs(data) {
    return request({
        url: '/node/flow/copyOrSaveAs',
        method: 'post',
        data,
        isLoading: true
    })
}

// 修改文件名称
export function updateFileName(data) {
    return request({
        url: '/node/flow/updateFileName',
        method: 'post',
        data
    })
}

// 保存节点文件
export function saveNodeFile(data) {
    return request({
        url: '/node/flow/saveNodeFile',
        method: 'post',
        data
    })
}

//根据nodeId删除
export function deleteNodeById(id) {
    return request({
        url: `/node/flow/deleteNodeById/${id}`,
        method: 'get'
    })
}

// base64转file
export function base64ToFile(data) {
    return request({
        url: '/node/flow/base64ToFile',
        method: 'post',
        data
    })
}

// 获取节点流控制变量详情
export function getNodeFlowDetail(data) {
    return request({
        url: '/task/getNodeFlowDetail',
        method: 'post',
        data
    })
}

// 生成任务
export function taskRun(data) {
    return request({
        url: '/task/taskRun',
        method: 'post',
        data
    })
}

// 上传图片
export function fileObsUpload(data) {
    return request({
        url: '/obs/file/obsUpload',
        method: 'post',
        data
    })
}

// 工具市场--查询已发布的节点流
export function findReleaseList(data) {
    return request({
        url: '/node/flow/findReleaseList',
        method: 'get',
        data
    })
}
