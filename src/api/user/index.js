import request from '../request'

// 用户基本信息
export function baseUserInfoById(data) {
    return request({
        url: '/user/baseUserInfoById',
        method: 'get',
        data
    })
}

// 修改密码
export function updatePassword(data) {
    return request({
        url: '/user/updatePassword',
        method: 'post',
        data
    })
}

// 由于忘记密码, 重置密码
export function resetPassword(data) {
    return request({
        url: '/user/resetPassword',
        method: 'post',
        data
    })
}

// 新账号注册
export function registryUser(data) {
    return request({
        url: '/user/registryUser',
        method: 'post',
        data
    })
}
