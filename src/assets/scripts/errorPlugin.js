import ChatBot from './robot.js'
import { throttle } from './util.js'

export const errorHandler = (err, vm, info) => {
    let token = sessionStorage.getItem('AI_TOKEN') || null
    if (!token) return
    let errInfo = null
    if (info !== 'JS错误' || info !== 'Promise错误') {
        errInfo = `
        --infoType: vue异常错误
        --apName : 用户端-${process.env.NODE_ENV === 'development' ? '测试环境' : '生产环境'}
        --url: ${window.location.href}
        --browser:${detectOS()}-${digits()} ${getBrowserInfo()}
        --time: ${format('yyyy-MM-dd hh:mm:ss')}
        --userInfo: ${sessionStorage.getItem('AI_INFO')}
        --errorInfo: ${err}-${JSON.stringify(info)}
        `
    } else {
        errInfo = err
    }
    // 将捕获的错误, 通过钉钉报警
    robotDD(errInfo)
}

const robotDD = (errMsg) => {
    const robot = new ChatBot({
        webhook: 'https://oapi.dingtalk.com/robot/send?access_token=8780e665c961a2bb64042199fb30328d99e92ba982c2ccc681b3ef1c633431e1',
        secret: 'SEC76ce85dd6fd8fd41a208dd4c3bbac67fbc563e9da2b84c2743e2f87f24998576'
    })
    // 规定发送的消息的类型和参数
    let textContent = {
        msgtype: 'text',
        text: {
            content: errMsg // 注意了，字符串里面的错误汉字，其实就是你在钉钉报警设置的自定义字段，两个地方需要相同，否则不会发送到群里
        },
        at: '15822371653'
    }
    // 机器人发送消息
    robot
        .send(textContent)
        .then((res) => {
            console.error(res)
        })
        .catch((error) => {
            ElMessage.error({
                message: `钉钉报警-${error.message}`
            })
            console.log('error', error)
        })
}
export const format = (fmt) => {
    //author: meizz
    var o = {
        'M+': new Date().getMonth() + 1, //月份
        'd+': new Date().getDate(), //日
        'h+': new Date().getHours(), //小时
        'm+': new Date().getMinutes(), //分
        's+': new Date().getSeconds(), //秒
        'q+': Math.floor((new Date().getMonth() + 3) / 3), //季度
        S: new Date().getMilliseconds() //毫秒
    }

    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (new Date().getFullYear() + '').substr(4 - RegExp.$1.length))
    for (var k in o) if (new RegExp('(' + k + ')').test(fmt)) fmt = fmt.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ('00' + o[k]).substr(('' + o[k]).length))
    return fmt
}

/**
 * 初始化设备信息
 */
export const initDeviceInfo = () => {
    let _deviceInfo = '' //设备信息
    console.log(navigator, 'navigator')
    if (navigator == null) {
        _deviceInfo = 'PC'
    }
    if (navigator.userAgent != null) {
        var su = navigator.userAgent.toLowerCase(),
            mb = ['ipad', 'iphone os', 'midp', 'rv:1.2.3.4', 'ucweb', 'android', 'windows ce', 'windows mobile']
        // 开始遍历提前设定好的设备关键字，如果设备信息中包含关键字，则说明是该设备
        for (var i in mb) {
            if (su.indexOf(mb[i]) > 0) {
                _deviceInfo = mb[i]
                break
            }
        }
    }
    return _deviceInfo
}

/**
 * 获取浏览器的信息
 */

export const getBrowserInfo = () => {
    var output = 'other'
    // Opera 8.0+
    var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0
    if (isOpera) {
        output = 'Opera'
    }
    // Firefox 1.0+
    var isFirefox = typeof InstallTrigger !== 'undefined'
    if (isFirefox) {
        output = 'Firefox'
    }
    // Safari 3.0+ "[object HTMLElementConstructor]"
    var isSafari =
        /constructor/i.test(window.HTMLElement) ||
        (function (p) {
            return p.toString() === '[object SafariRemoteNotification]'
        })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification))
    if (isSafari) {
        output = 'Safari'
    }
    // Internet Explorer 6-11
    var isIE = /*@cc_on!@*/ false || !!document.documentMode
    if (isIE) {
        output = 'IE'
    }
    // Edge 20+
    var isEdge = !isIE && !!window.StyleMedia
    if (isEdge) {
        output = 'Edge'
    }
    // Chrome 1 - 79
    var isChrome = !!window.chrome && navigator.userAgent.indexOf('Chrome') !== -1
    if (isChrome) {
        output = 'Chrome'
    }
    // Edge (based on chromium) detection
    var isEdgeChromium = isChrome && navigator.userAgent.indexOf('Edg') !== -1
    if (isEdgeChromium) {
        output = 'EdgeChromium'
    }
    return output
}

export const detectOS = () => {
    var userAgent = window.navigator.userAgent,
        platform = window.navigator.platform,
        macosPlatforms = ['Macintosh', 'MacIntel', 'MacPPC', 'Mac68K'],
        windowsPlatforms = ['Win32', 'Win64', 'Windows', 'WinCE'],
        iosPlatforms = ['iPhone', 'iPad', 'iPod'],
        os = null

    if (macosPlatforms.indexOf(platform) !== -1) {
        os = 'Mac OS'
    } else if (iosPlatforms.indexOf(platform) !== -1) {
        os = 'iOS'
    } else if (windowsPlatforms.indexOf(platform) !== -1) {
        os = 'Windows'
    } else if (/Android/.test(userAgent)) {
        os = 'Android'
    } else if (!os && /Linux/.test(platform)) {
        os = 'Linux'
    }
    return os
}

export const digits = () => {
    var sUserAgent = navigator.userAgent
    var is64 = sUserAgent.indexOf('WOW64') > -1
    if (is64) {
        return '64bit'
    } else {
        return '32bit'
    }
}
