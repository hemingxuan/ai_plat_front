export const defaultGraph = {
	"last_node_id": 51,
	"last_link_id": 71,
	"nodes": [
	  {
		"id": 15,
		"type": "CLIPTextEncode",
		"pos": [
		  456,
		  82
		],
		"size": [
		  400,
		  200
		],
		"flags": {},
		"order": 4,
		"mode": 0,
		"inputs": [
		  {
			"name": "clip模型",
			"type": "CLIP",
			"link": 65
		  },
		  {
			"name": "文本",
			"type": "STRING",
			"link": 68,
			"widget": {
			  "name": "文本",
			  "config": [
				"STRING",
				{
				  "multiline": true
				}
			  ]
			}
		  }
		],
		"outputs": [
		  {
			"name": "条件信息",
			"type": "CONDITIONING",
			"links": [
			  63
			],
			"shape": 3,
			"slot_index": 0
		  }
		],
		"properties": {
		  "Node name for S&R": "CLIPTextEncode"
		},
		"widgets_values": [
		  "high school girl"
		]
	  },
	  {
		"id": 49,
		"type": "UIInputMultilineStringNode",
		"pos": [
		  -186,
		  390
		],
		"size": {
		  "0": 400,
		  "1": 200
		},
		"flags": {},
		"order": 0,
		"mode": 0,
		"outputs": [
		  {
			"name": "文本",
			"type": "STRING",
			"links": [
			  67
			],
			"shape": 3,
			"slot_index": 0
		  }
		],
		"properties": {
		  "Node name for S&R": "UIInputMultilineStringNode"
		},
		"widgets_values": [
		  "bad hand,bad face, ugly, nsfw"
		]
	  },
	  {
		"id": 16,
		"type": "CLIPTextEncode",
		"pos": [
		  459,
		  364
		],
		"size": [
		  400,
		  200
		],
		"flags": {},
		"order": 5,
		"mode": 0,
		"inputs": [
		  {
			"name": "clip模型",
			"type": "CLIP",
			"link": 66,
			"slot_index": 0
		  },
		  {
			"name": "文本",
			"type": "STRING",
			"link": 67,
			"widget": {
			  "name": "文本",
			  "config": [
				"STRING",
				{
				  "multiline": true
				}
			  ]
			}
		  }
		],
		"outputs": [
		  {
			"name": "条件信息",
			"type": "CONDITIONING",
			"links": [
			  20
			],
			"shape": 3,
			"slot_index": 0
		  }
		],
		"properties": {
		  "Node name for S&R": "CLIPTextEncode"
		},
		"widgets_values": [
		  "ugly"
		]
	  },
	  {
		"id": 13,
		"type": "VAEDecode",
		"pos": [
		  1339,
		  178
		],
		"size": {
		  "0": 210,
		  "1": 46
		},
		"flags": {},
		"order": 8,
		"mode": 0,
		"inputs": [
		  {
			"name": "经采样的隐信息",
			"type": "LATENT",
			"link": 23
		  },
		  {
			"name": "VAE模型",
			"type": "VAE",
			"link": 12,
			"slot_index": 1
		  }
		],
		"outputs": [
		  {
			"name": "图像",
			"type": "IMAGE",
			"links": [
			  14
			],
			"shape": 3,
			"slot_index": 0
		  }
		],
		"properties": {
		  "Node name for S&R": "VAEDecode"
		}
	  },
	  {
		"id": 50,
		"type": "LoadImage",
		"pos": [
		  -106,
		  664
		],
		"size": [
		  315,
		  314
		],
		"flags": {},
		"order": 1,
		"mode": 0,
		"outputs": [
		  {
			"name": "图像",
			"type": "IMAGE",
			"links": [
			  70
			],
			"shape": 3
		  },
		  {
			"name": "蒙版",
			"type": "MASK",
			"links": null,
			"shape": 3
		  }
		],
		"properties": {
		  "Node name for S&R": "LoadImage"
		},
		"widgets_values": [
		  "test (1).jpg",
		  "image"
		]
	  },
	  {
		"id": 51,
		"type": "VAEEncode",
		"pos": [
		  600,
		  657
		],
		"size": {
		  "0": 210,
		  "1": 46
		},
		"flags": {},
		"order": 6,
		"mode": 0,
		"inputs": [
		  {
			"name": "图像",
			"type": "IMAGE",
			"link": 70,
			"slot_index": 0
		  },
		  {
			"name": "VAE模型",
			"type": "VAE",
			"link": 69,
			"slot_index": 1
		  }
		],
		"outputs": [
		  {
			"name": "隐信息",
			"type": "LATENT",
			"links": [
			  71
			],
			"shape": 3,
			"slot_index": 0
		  }
		],
		"properties": {
		  "Node name for S&R": "VAEEncode"
		}
	  },
	  {
		"id": 48,
		"type": "UIInputMultilineStringNode",
		"pos": [
		  -191,
		  -51
		],
		"size": {
		  "0": 400,
		  "1": 200
		},
		"flags": {},
		"order": 2,
		"mode": 0,
		"outputs": [
		  {
			"name": "文本",
			"type": "STRING",
			"links": [
			  68
			],
			"shape": 3,
			"slot_index": 0
		  }
		],
		"properties": {
		  "Node name for S&R": "UIInputMultilineStringNode"
		},
		"widgets_values": [
		  "color photo of beauty, delicate features, soft natural light and bokeh, peaceful and ethereal environment — Nikon D850, Kodak Portra 800 film, 85mm lens, shallow depth of field and exposure compensation — Wong Kar-wai, Roger Deakins, Annie Leibovitz, Valentino —c 10 —ar 2:3"
		]
	  },
	  {
		"id": 10,
		"type": "CheckpointLoaderSimple",
		"pos": [
		  -116,
		  223
		],
		"size": {
		  "0": 315,
		  "1": 98
		},
		"flags": {},
		"order": 3,
		"mode": 0,
		"outputs": [
		  {
			"name": "采样模型",
			"type": "MODEL",
			"links": [
			  64
			],
			"shape": 3,
			"slot_index": 0
		  },
		  {
			"name": "CLIP模型",
			"type": "CLIP",
			"links": [
			  65,
			  66
			],
			"shape": 3,
			"slot_index": 1
		  },
		  {
			"name": "VAE模型",
			"type": "VAE",
			"links": [
			  12,
			  69
			],
			"shape": 3
		  }
		],
		"properties": {
		  "Node name for S&R": "CheckpointLoaderSimple"
		},
		"widgets_values": [
		  "20230418_HuzhouZhili/20230418_HuzhouZhili_109700.safetensors"
		]
	  },
	  {
		"id": 14,
		"type": "PreviewImage",
		"pos": [
		  1592,
		  179
		],
		"size": {
		  "0": 378,
		  "1": 404
		},
		"flags": {},
		"order": 9,
		"mode": 0,
		"inputs": [
		  {
			"name": "图像",
			"type": "IMAGE",
			"link": 14
		  }
		],
		"properties": {
		  "Node name for S&R": "PreviewImage"
		}
	  },
	  {
		"id": 19,
		"type": "KSampler",
		"pos": [
		  970,
		  178
		],
		"size": {
		  "0": 315,
		  "1": 262
		},
		"flags": {},
		"order": 7,
		"mode": 0,
		"inputs": [
		  {
			"name": "采样模型",
			"type": "MODEL",
			"link": 64,
			"slot_index": 0
		  },
		  {
			"name": "正向条件",
			"type": "CONDITIONING",
			"link": 63
		  },
		  {
			"name": "负向条件",
			"type": "CONDITIONING",
			"link": 20
		  },
		  {
			"name": "图像隐信息",
			"type": "LATENT",
			"link": 71,
			"slot_index": 3
		  }
		],
		"outputs": [
		  {
			"name": "经采样的隐信息",
			"type": "LATENT",
			"links": [
			  23
			],
			"shape": 3,
			"slot_index": 0
		  }
		],
		"properties": {
		  "Node name for S&R": "KSampler"
		},
		"widgets_values": [
		  373496041882880,
		  "randomize",
		  20,
		  8,
		  "euler",
		  "normal",
		  1
		]
	  }
	],
	"links": [
	  [
		12,
		10,
		2,
		13,
		1,
		"VAE"
	  ],
	  [
		14,
		13,
		0,
		14,
		0,
		"IMAGE"
	  ],
	  [
		20,
		16,
		0,
		19,
		2,
		"CONDITIONING"
	  ],
	  [
		23,
		19,
		0,
		13,
		0,
		"LATENT"
	  ],
	  [
		63,
		15,
		0,
		19,
		1,
		"CONDITIONING"
	  ],
	  [
		64,
		10,
		0,
		19,
		0,
		"MODEL"
	  ],
	  [
		65,
		10,
		1,
		15,
		0,
		"CLIP"
	  ],
	  [
		66,
		10,
		1,
		16,
		0,
		"CLIP"
	  ],
	  [
		67,
		49,
		0,
		16,
		1,
		"STRING"
	  ],
	  [
		68,
		48,
		0,
		15,
		1,
		"STRING"
	  ],
	  [
		69,
		10,
		2,
		51,
		1,
		"VAE"
	  ],
	  [
		70,
		50,
		0,
		51,
		0,
		"IMAGE"
	  ],
	  [
		71,
		51,
		0,
		19,
		3,
		"LATENT"
	  ]
	],
	"groups": [],
	"config": {},
	"extra": {},
	"version": 0.4
  }
