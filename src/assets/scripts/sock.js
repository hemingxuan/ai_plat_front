import SockJS from 'sockjs-client/dist/sockjs.min.js'
import Stomp from 'stompjs'
import ElMessage from 'element-plus'

let sock = {
    socket: null,
    socketUrl: `${import.meta.env.VITE_BASE_API}/ui`,
    reconnecting: false,
    headers: {}, // 定义客户端的认证信息,按需求配置 (后期用于传 cookie 信息)
    initWebSocket(callback, pageType) {
        // 建立连接对象
        //连接服务端提供的通信接口，连接以后才可以订阅广播消息和个人消息
        this.socket = new SockJS(this.socketUrl)
        // 获取STOMP子协议的客户端对象
        this.stompClient = Stomp.over(this.socket)
        // 向服务器发起websocket连接
        this.stompClient.connect(
            this.headers,
            (frame) => {
                // 订阅服务端提供的某个topic
                this.connectSucceed(callback, pageType)
            },
            (err, callback) => {
                // 连接发生错误时的处理函数
                console.log(err)
                this.reconnect(callback, pageType)
            }
        )
    },
    // 连接成功的回调：订阅服务器的地址。为了浏览器可以接收到消息，必须先订阅服务器的地址
    connectSucceed(callback, pageType) {
        if (pageType === 'taskTool') {
            const userId = sessionStorage.getItem('AI_USERID')
            this.stompClient.subscribe(`/user/${userId}/msg`, (msg) => {
                console.log('msg.body', msg.body)
                callback(JSON.parse(msg.body))
            })
        } else if (pageType === 'trainData') {
            // TODO
        }
        /* 
            当客户端与服务端连接成功后，可以调用 send()来发送STOMP消息。这个方法必须有一个参数，用来描述对应的STOMP的目的地。
            另外可以有两个可选的参数：headers，object类型包含额外的信息头部；body，一个String类型的参数。

            例如：client.send("/queue/test", {priority: 9}, "Hello, STOMP");
            client会发送一个STOMP发送帧给/queue/test，这个帧包含一个设置了priority为9的header和内容为“Hello, STOMP”的body。
        */
        // 用于后期客户端向服务端发送消息
        // this.stompClient.send('/topic/dashboard/send',{}, '1')
    },
    // 重新连接
    reconnect(callback, pageType) {
        let that = this
        this.reconnecting = true
        let connected = false
        const timer = setInterval(() => {
            that.socket = new SockJS(this.socketUrl)
            that.stompClient = Stomp.over(this.socket)
            that.stompClient.connect(
                that.headers,
                (frame) => {
                    that.reconnectting = false
                    connected = true
                    clearInterval(timer)
                    that.connectSucceed(callback, pageType)
                },
                (err) => {
                    console.log('Reconnect failed！')
                    if (!connected) {
                        console.log(err)
                        callback('错误')
                        ElMessage.error({
                            message: err || '连接失败, 请刷新后重试'
                        })
                    }
                }
            )
        }, 1000)
    },
    // 断开连接
    close() {
        if (this.stompClient != null) {
            this.stompClient.disconnect()
            console.log('Disconnected')
        }
    }
}

export default sock
