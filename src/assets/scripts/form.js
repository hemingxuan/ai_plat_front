// 真实姓名校验
export const validateRealName = (rule, value, callback) => {
    const pwdRegex = /[\u4e00-\u9fa5]+/
    const enReg = /[`~!@#$%^&*()_+<>?:"{},\/;'[\]]/im
    const chReg = /[·！#￥（——）：；“”‘、，|《。》？、【】[\]]/im
    if (value === '') {
        callback(new Error('请输入真实姓名'))
    } else if (!pwdRegex.test(value)) {
        callback(new Error('请输入中文简繁体字符或者特殊字符只能输入.'))
    } else if (enReg.test(value) || chReg.test(value)) {
        callback(new Error('特殊字符只能输入.'))
    } else {
        callback()
    }
}

// 手机号输入
export const validateMobile = (rule, value, callback) => {
    let regu = /^1[3-9][0-9]{9}$/
    if (value === '') {
        callback(new Error('请输入手机号'))
    } else if (!regu.test(value)) {
        callback(new Error('请输入正确的手机号')) // 手机号校验
    } else {
        callback()
    }
}
// 第一次输入密码校验
export const validatePass = (rule, value, callback) => {
    const pwdRegex = /[\u4e00-\u9fa5]+/
    const chReg = /[·！#￥（——）：；“”‘、，|《。》？、【】[\]]/im
    if (value === '') {
        callback(new Error('请输入密码'))
    } else if (pwdRegex.test(value) || chReg.test(value)) {
        callback(new Error('密码中不能存在中文及中文特殊字符, 且至少1个字符, 最多30个字符'))
    } else {
        callback()
    }
}
