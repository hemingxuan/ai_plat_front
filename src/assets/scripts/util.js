let debounceTimer = null,
    throttleTimer = null
/**
 * 生成一个不重复的ID
 */
// 引入时间戳 随机数前置 36进制 加入随机数长度控制 v0.1.3
export function GenNonDuplicateID(randomLength) {
    return Number(Math.random().toString().substr(3, randomLength) + Date.now()).toString(36)
}

// 防抖
export const debounce = (fn, delay) => {
    return (...args) => {
        if (debounceTimer) {
            clearTimeout(debounceTimer)
        }
        debounceTimer = setTimeout(() => {
            fn.apply(this, args)
        }, delay)
    }
}
// 节流
export const throttle = (fn, delay) => {
    return (...args) => {
        if (throttleTimer) {
            return
        }
        throttleTimer = setTimeout(() => {
            fn.apply(this, args)
            throttleTimer = null
        }, delay)
    }
}
/**
 * 参数拼接
 * @param {object} obj 只支持非嵌套的对象
 * @returns {string}
 */
export const params = (obj) => {
    let result = ''
    let item
    for (item in obj) {
        if (obj[item] && String(obj[item])) {
            result += `&${item}=${obj[item]}`
        }
    }
    if (result) {
        result = '?' + result.slice(1)
    }
    return result
}
