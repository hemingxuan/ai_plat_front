import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Asset from '../views/assetManage/index.vue'
import { usePersistStore } from '@/store/persist.js'
const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/',
            component: Home,
            redirect: '/list',
            children: [
                {
                    path: '/list',
                    component: () => import('../views/blueprint/List.vue'),
                    meta: {
                        title: '蓝图列表',
                        capture: true
                    }
                },
                {
                    path: '/node',
                    component: () => import('../views/blueprint/Node.vue'),
                    meta: {
                        title: '节点流'
                    }
                },
                {
                    path: '/userCenter',
                    component: () => import('../views/account/userCenter.vue'),
                    meta: {
                        title: '个人设置'
                    }
                },
                {
                    path: '/blueprint/marketTool',
                    component: () => import('@/views/blueprint/marketTool.vue'),
                    meta: {
                        title: '工具市场',
                        requiresAuth: false // 不校验用户身份，用户可直接访问的路由
                    }
                },
                {
                    path: '/assetManage',
                    component: Asset,
                    redirect: '/assetManage/square',
                    children: [
                        {
                            path: '/assetManage/square',
                            component: () => import('../views/assetManage/square.vue'),
                            meta: {
                                title: '资产广场'
                            }
                        },
                        {
                            path: '/assetManage/uploadAsset',
                            component: () => import('../views/assetManage/uploadAsset.vue'),
                            meta: {
                                title: '上传资产'
                            }
                        },
                        {
                            path: '/assetManage/styleTraining',
                            component: () => import('../views/assetManage/styleTraining.vue'),
                            meta: {
                                title: '风格训练'
                            }
                        },
                        {
                            path: '/assetManage/myAssets',
                            component: () => import('../component/myAssets.vue'),
                            meta: {
                                title: '我的资产'
                            }
                        }
                    ]
                }
            ]
        },
        {
            path: '/login',
            component: () => import('@/views/account/login.vue'),
            meta: {
                title: '登录',
                requiresAuth: false // 不校验用户身份，用户可直接访问的路由
            }
        },
        {
            path: '/personal/register',
            component: () => import('@/views/account/register.vue'),
            meta: {
                title: '注册',
                requiresAuth: false // 不校验用户身份，用户可直接访问的路由
            }
        },
        {
            path: '/blueprint/home',
            component: () => import('@/views/blueprint/Home.vue'),
            meta: {
                title: '上传任务',
                requiresAuth: false // 不校验用户身份，用户可直接访问的路由
            }
        },
        {
            path: '/modifyPassword',
            component: () => import('../views/account/Modify.vue'),
            meta: {
                title: '修改密码',
                requiresAuth: false // 不校验用户身份，用户可直接访问的路由
            }
        },
        {
            path: '/:pathMatch(.*)*', // 找不到指定页面
            redirect: '/list'
        }
    ]
})

// 全局路由守卫
router.beforeEach((to, from, next) => {
    // 无需验证用户身份
    if (to.meta.requiresAuth === false) next()
    else {
        // 需要验证用户身份
        // const token = sessionStorage.getItem('AI_TOKEN') || null
        const storePersist = usePersistStore()
        const token = storePersist.token
        document.title = to.meta.title
        if (token) {
            if (to.path === '/login') return next('/')
            next()
        } else {
            if (to.path === '/login') return next()
            next('/login')
        }
    }
})

export default router
