import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'
import eslintPlugin from 'vite-plugin-eslint'
const timestamp = new Date().getTime()

export default defineConfig(({ mode }) => {
    return {
        base: '/',
        plugins: [
            vue(),
            eslintPlugin({
                include: ['src/**/*.vue', 'src/*.js', 'src/*.vue']
            }),
            AutoImport({
                resolvers: [ElementPlusResolver()]
            }),
            Components({
                resolvers: [ElementPlusResolver()]
            })
        ],
        resolve: {
            alias: {
                '@': fileURLToPath(new URL('./src', import.meta.url))
            }
        },
        build: {
            outDir: `${mode === 'pro' ? 'build' : 'dist'}`,
            rollupOptions: {
                output: {
                    manualChunks: (filePath) => {
                        if (filePath.includes('node_modules')) {
                            return 'vendor'
                        }
                    },
                    entryFileNames: `assets/[name].${timestamp}.js`,
                    // 块文件名
                    chunkFileNames: `assets/[name]-[hash].${timestamp}.js`,
                    // 资源文件名 css 图片等等
                    assetFileNames: `assets/[name]-[hash].${timestamp}.[ext]`
                }
            }
        },
        server: {
            host: '0.0.0.0',
            port: 8080,
            proxy: {
                '/api': {
                    target: 'https://oapi.dingtalk.com', // 代理地址
                    changeOrigin: true, // 是否允许跨域，为true代表允许
                    rewrite: (path) => path.replace(/^\/api/, '')
                }
            }
        }
    }
})
